{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards  #-}

module Mtst2 where



import qualified Data.Text as T

import Control.Monad.Trans.Except
import Control.Monad.State

import Data.List
import Data.Maybe

import Language.Haskell.TH

import qualified Data.Map as DM
import qualified Data.Set as DS

import System.FilePath

import Control.Monad

import Lib

type MySt = DS.Set String

type MyQ a = StateT MySt Q a

tshow :: (Show a) => a -> String
tshow = tail . takeExtension . show

renderAgD :: AgD -> String
renderAgD = show
-- renderAgD (AgD {agdName = "GHC.Base."}) = undefined

render :: DS.Set AgD -> String
render x = intercalate "\n\n" $ map renderAgD $ DS.toList x

hask2ag'' :: String -> Q Exp
hask2ag'' typ = do
  Just tnam <- lookupTypeName typ
  let ppq = flip runStateT mempty $ hask2ag tnam
  pp2 <- runQ ppq
  let pp3 = DS.filter (\x -> not $ "GHC"  `isInfixOf` (show $ getAgDName x) ) $ snd pp2
  let pp = fixAgts (DS.toList pp3,[])
  let gg = (intercalate "\n\n" $ map show $ DS.toList pp3) ++ "\n\n----====----====---\n\n" ++ (render $ DS.fromList pp)
  [e|gg|]

prepareTyp :: Type -> StateT (DS.Set AgD) Q ()
prepareTyp (ConT c) = hask2ag c
prepareTyp (ListT) = return ()
prepareTyp (VarT _) = return ()
prepareTyp (TupleT _) = return ()
prepareTyp (AppT a b) = do
  prepareTyp a
  prepareTyp b
prepareTyp t = error $ show ("preparetyp", t)

prepareCon :: Con -> StateT (DS.Set AgD) Q ()
prepareCon (NormalC cnam cbtypes) = do
  let typs = map snd cbtypes
  mapM prepareTyp typs
  hask2ag cnam
prepareCon t = error $ show ("prepareCon", t)


hask2ag :: Name -> StateT (DS.Set AgD) Q ()
hask2ag tnam = do
  rnam <- lift $ reify tnam

  case rnam of
    TyConI tycon -> do
      case tycon of
        DataD _ dnam tyvars _ cns _ -> do
          mapM prepareCon cns
          let a = AgD {agdName = dnam, agdArgs = tyvars, agdCons = cns}
          pushDataInfo a
        NewtypeD _ dnam tyvars _ cn _ -> do
          prepareCon cn
          let a = AgD {agdName = dnam, agdArgs = tyvars, agdCons = [cn]}
          pushDataInfo a
        TySynD dnam tyvars ttyp -> do
          prepareTyp ttyp
          let a = AgT {agtName = dnam, agtArgs = tyvars, agtUnderlyingType = linearizeAppT ttyp}
          pushDataInfo a
        _ -> error $ show ("wow implement",tycon)
    PrimTyConI pnam _ _ -> do
      let a = AgPrim {agpName = pnam}
      pushDataInfo a
    DataConI _ _ _ -> do
      return ()
    _ -> error $ show ("wow2 implement",rnam)
  return ()

pushDataInfo :: AgD -> StateT (DS.Set AgD) Q ()
pushDataInfo a = do
  modify (\x -> DS.insert a x)

data AgCon = AgCon {agcName :: String, agcImpli :: [String],agcArgCon :: [LinearType]}

data AgD =
    AgD {agdName :: Name , agdArgs :: [TyVarBndr], agdCons :: [Con]}
  | AgT {agtName :: Name , agtArgs :: [TyVarBndr], agtUnderlyingType :: LinearType}
  | AgPrim {agpName :: Name}
  deriving Show

getAgDName (AgD {..}) = agdName
getAgDName (AgT {..}) = agtName
getAgDName (AgPrim {..}) = agpName

data CustomName = OrigName Name | CustomTuple2 | CustomList

instance Show CustomName where
  show (OrigName x) = "O@" ++ show x
  show (CustomTuple2) = "T2@"
  show (CustomList) = "L@"
  

data LinearType = LinearType CustomName [LinearType] | LF CustomName

instance Show LinearType where
  show (LinearType x xs) = "(" ++ show x ++ ":: " ++ (intercalate " → " $ map show xs) ++ ")"
  show (LF x) = "||"++show x

linearizeAppT :: Type -> LinearType
linearizeAppT (VarT a) = LF $ OrigName a
linearizeAppT (ConT a) = LF $ OrigName a
linearizeAppT (TupleT 2) = LF $ CustomTuple2
linearizeAppT (ListT) = LF $ CustomList
linearizeAppT (AppT a b) = case linearizeAppT a of
  LinearType x xs -> LinearType x (xs++[linearizeAppT b])
  LF x -> LinearType x [linearizeAppT b]
linearizeAppT t = error $ show ("linearizeappt",t)

-- convertTypeToData :: AgD -> StateT (DS.Set AgD) Q AgD
-- convertTypeToData t@(AgD {..}) = return t
-- convertTypeToData t@(AgPrim {..}) = return t
-- convertTypeToData t@(AgT {..}) = do
--   undefined

instance Eq (AgD) where
  a == b = getAgDName a == getAgDName b
  
instance Ord (AgD) where
  compare (a) (b) = (compare (getAgDName a) (getAgDName b)) 
  

fixAgts :: ([AgD],[AgD]) -> [AgD]
fixAgts ([],x) = x
fixAgts (x@(AgT {..}):xs,y) = case containsAgT (agtUnderlyingType) xs of
  Just j ->  fixAgts (xs++[x],y)
  Nothing -> case containsAgT (agtUnderlyingType) y of
    Just k -> fixAgts ((x {agtUnderlyingType = fixUnderly  (DS.fromList y) k agtUnderlyingType}):xs,y)
    Nothing -> fixAgts (xs,x:y)
fixAgts (x:xs,y) = fixAgts (xs,x:y)

fixUnderly :: DS.Set AgD  -> Name ->  LinearType -> LinearType
fixUnderly db n o@(LinearType lt lts) | and $ (isJust $ n `customNameEq` lt):(map (\zz -> not $ isJust $ containsAgT zz $ DS.toList db) lts) = let a = head $ DS.toList $ DS.filter (\y -> getAgDName y == n) db in  jmap lts (agtUnderlyingType a)
                                      | otherwise = LinearType lt $  map (fixUnderly db n) lts
fixUnderly db n o@(LF lt) | isJust $ n `customNameEq` lt = let a = head $ DS.toList $ DS.filter (\y -> getAgDName y == n) db in  (agtUnderlyingType a)
                          | otherwise = o


jmap lts (LinearType x y) = LinearType x lts

containsAgT :: LinearType ->  [AgD] -> Maybe Name
containsAgT a [] = Nothing
containsAgT o@(LF a) (x:xs) = case  customNameEq (getAgDName x) a of
  Just j -> Just j
  Nothing -> containsAgT o xs
containsAgT o@(LinearType a as) (x:xs) = case  filter isJust $ (customNameEq (getAgDName x) a):(map (flip containsAgT (x:xs)) as) of
  (Just k):_ -> Just k
  [] -> containsAgT o xs

customNameEq :: Name -> CustomName -> Maybe Name
customNameEq a (OrigName o) | a==o = Just o
customNameEq _ _ = Nothing

