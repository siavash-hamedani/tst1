{-# LANGUAGE TemplateHaskell #-}
module Agda.Syntax.Mtst1 where


import qualified Data.Text as T

import Control.Monad.Trans.Except
import Control.Monad.State

import Data.Data
import Data.List

import Control.Monad.State

import Language.Haskell.TH

import Mtst2

import Lib

import qualified Data.Set as DS

main :: IO ()
main = do
  putStrLn  $(hask2ag'' "Lib.Expr"   )
